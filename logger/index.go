package logger

import (
	"io"
	"os"

	"gitlab.com/phummarin.dam/utility/chrono"

	log "github.com/sirupsen/logrus"
)

var (
	logger *log.Entry
)

func Init(appName string) error {
	log.SetFormatter(LogFormatter())
	log.SetLevel(LogLevel())
	log.SetOutput(io.MultiWriter(os.Stdout))
	logger = log.StandardLogger().WithField("application", appName)
	return nil
}

func Logger() *log.Entry {
	return logger
}

func LogFormatter() *log.JSONFormatter {
	return &log.JSONFormatter{
		TimestampFormat: string(chrono.Format_ISO8601),
	}
}

func LogLevel() log.Level {
	logLevel, err := log.ParseLevel("info")
	if err != nil {
		panic(err)
	}
	return logLevel
}
