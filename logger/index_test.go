package logger_test

import (
	"testing"

	"gitlab.com/phummarin.dam/utility/logger"
)

func TestInit(t *testing.T) {
	err := logger.Init("TEST")
	if err != nil {
		t.Errorf("TestInit: %v\n", err)
		return
	}
}

func TestLogger(t *testing.T) {
	log := logger.Logger()
	if log == nil {
		t.Errorf("Log nil")
		return
	}
}

func TestLogFormatter(t *testing.T) {
	logFormat := logger.LogFormatter()
	if logFormat == nil {
		t.Errorf("Log nil")
		return
	}
}

func TestLogLevel(t *testing.T) {
	logLevel := logger.LogLevel()
	if logLevel == 0 {
		t.Errorf("logLevel = 0")
		return
	}
}
