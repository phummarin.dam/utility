package http

import (
	"bytes"
	"encoding/json"
	"net/http"

	"gitlab.com/phummarin.dam/utility/model"
	"gitlab.com/phummarin.dam/utility/repositories"
)

type RequestService struct {
	Client repositories.HttpClientInterface
}

func (c RequestService) SendAPI(httpParam model.HttpRequestParam) (*http.Response, error) {
	jsonStr, err := json.Marshal(httpParam.Request)
	if err != nil {
		return nil, err
	}
	request, _ := http.NewRequest(httpParam.Method, httpParam.Url, bytes.NewBuffer(jsonStr))
	request.Header.Set("Content-Type", httpParam.ContentType)
	request.Header.Set("x-api-key", httpParam.ApiKey)
	resp, err := c.Client.Do(request)
	if err != nil {
		return nil, err
	}
	return resp, nil
}
