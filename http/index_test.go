package http_test

import (
	"errors"
	"net/http"
	"testing"

	https "gitlab.com/phummarin.dam/utility/http"
	"gitlab.com/phummarin.dam/utility/model"
	"gitlab.com/phummarin.dam/utility/repositories"
)

func TestSendAPI(t *testing.T) {
	type argsMock struct {
		res *http.Response
		err error
	}
	type args struct {
		httpParam model.HttpRequestParam
	}
	tests := []struct {
		name    string
		inParam *args
		inMock  *argsMock
		want    *http.Response
		wantErr bool
	}{
		// TODO: Add test cases.
		{
			name: "case success",
			inParam: &args{model.HttpRequestParam{
				Method:      "PUT",
				Url:         "localhost",
				Request:     "{'test':'1'}",
				ContentType: "application/json",
				ApiKey:      "12345",
			}},
			inMock:  &argsMock{&http.Response{StatusCode: http.StatusOK}, nil},
			want:    nil,
			wantErr: false,
		},
		{
			name: "case Marshal fail",
			inParam: &args{model.HttpRequestParam{
				Method:      "PUT",
				Url:         "localhost",
				Request:     "{'test':'1'}",
				ContentType: "application/json",
				ApiKey:      "12345",
			}},
			inMock:  &argsMock{nil, errors.New("Error")},
			want:    nil,
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			client := &repositories.HttpClientInterfaceMock{
				DoFunc: func(req *http.Request) (*http.Response, error) {
					// do whatever you want
					return tt.inMock.res, tt.inMock.err
				},
			}
			requestH := https.RequestService{Client: client}
			_, err := requestH.SendAPI(tt.inParam.httpParam)
			if (err != nil) != tt.wantErr {
				t.Errorf("TestSendAPI name = %v,Error %v, wantErr %v", tt.name, err, tt.wantErr)
				return
			}
		})
	}
}
