package repositories

import (
	"net/http"

	"github.com/couchbase/gocb"
)

type ClusterInterface interface {
	Close() error
	Authenticate(auth gocb.Authenticator) error
	OpenBucket(bucket, password string) (*gocb.Bucket, error)
}

type BucketInterface interface {
	ExecuteN1qlQuery(q *gocb.N1qlQuery, params interface{}) (gocb.QueryResults, error)
}

type HttpClientInterface interface {
	Do(req *http.Request) (*http.Response, error)
}
