package repositories

import (
	"net/http"

	"github.com/couchbase/gocb"
	"github.com/stretchr/testify/mock"
)

type ClusterInterfaceMock struct {
	mock.Mock
}

func (m *ClusterInterfaceMock) OpenBucket(bucket, password string) (*gocb.Bucket, error) {
	arguments := m.Called(bucket, password)
	return arguments.Get(0).(*gocb.Bucket), arguments.Error(1)
}

func (m *ClusterInterfaceMock) Close() error {
	arguments := m.Called()
	return arguments.Error(0)
}

func (m *ClusterInterfaceMock) Authenticate(auth gocb.Authenticator) error {
	arguments := m.Called(auth)
	return arguments.Error(0)
}

type BucketInterfaceMock struct {
	mock.Mock
}

func (m *BucketInterfaceMock) ExecuteN1qlQuery(q *gocb.N1qlQuery, params interface{}) (gocb.QueryResults, error) {
	arguments := m.Called(q, params)
	return arguments.Get(0).(gocb.QueryResults), arguments.Error(1)
}

type HttpClientInterfaceMock struct {
	DoFunc func(req *http.Request) (*http.Response, error)
}

func (m *HttpClientInterfaceMock) Do(req *http.Request) (*http.Response, error) {
	if m.DoFunc != nil {
		return m.DoFunc(req)
	}
	// just in case you want default correct return value
	return &http.Response{}, nil
}
