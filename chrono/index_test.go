package chrono

import (
	"testing"
	"time"
)

func TestSetDefaultZone(t *testing.T) {

	SetDefaultZone(Zone_Bangkok)
}

func TestNow(t *testing.T) {
	Now()
}

func TestTimestamp(t *testing.T) {
	Timestamp()
}

func TestMarshalJSONISO(t *testing.T) {
	SetDefaultZone(Zone_Bangkok)
	iso := ISO8601{}

	res, err := iso.MarshalJSON()
	if err != nil {
		t.Errorf("TestMarshalJSONISO: %v\n", err)
		return
	}
	iso2 := new(ISO8601)
	err = iso2.UnmarshalJSON(res)
	if err != nil {
		t.Errorf("TestUnmarshalJSONISO: %v\n", err)
		return
	}

}
func TestToUnixISO(t *testing.T) {
	SetDefaultZone(Zone_Bangkok)
	iso := ISO8601{}
	iso.ToUnix()
}
func TestMarshalJSONUNIX(t *testing.T) {
	SetDefaultZone(Zone_Bangkok)
	uni := Unix{}
	res, err := uni.MarshalJSON()
	if err != nil {
		t.Errorf("TestMarshalJSONUNIX: %v\n", err)
		return
	}

	uni2 := new(Unix)
	err = uni2.UnmarshalJSON(res)
	if err != nil {
		t.Errorf("TestUnmarshalJSONUNIX: %v\n", err)
		return
	}

}

func TestMarshalJSONTopValue(t *testing.T) {
	SetDefaultZone(Zone_Bangkok)
	top := TopValue{time.Now().Local()}
	_, err := top.MarshalJSON()
	if err != nil {
		t.Errorf("TestMarshalJSONTopValue: %v\n", err)
		return
	}

	b := []byte(`"` + time.Now().Local().Format("2006-01-02 15:04:05.000") + `"`)

	top1 := new(TopValue)
	err = top1.UnmarshalJSON(b)
	if err != nil {
		t.Errorf("TestUnmarshalJSONTopValue: %v\n", err)
		return
	}

}
