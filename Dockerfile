ARG BUILDER_IMAGE=golang:1.16-alpine
ARG BASE_IMAGE=alpine:latest

FROM $BUILDER_IMAGE AS builder

WORKDIR /app

COPY go.mod go.sum ./

RUN apk update && apk add git && apk --no-cache add tzdata
RUN go mod download

COPY . .

RUN go build -o main .


FROM $BASE_IMAGE as final

RUN apk --no-cache add ca-certificates
ENV PATH="/app/:${PATH}"

WORKDIR /app/

ARG TIMEZONE="Asia/Bangkok"

# Set timezone
RUN apk add tzdata
RUN cp /usr/share/zoneinfo/${TIMEZONE} /etc/localtime
RUN "date"

RUN apk update && apk add git

# Copy the Pre-built binary file from the previous stage
COPY --from=builder /app/main .

ENTRYPOINT ["./main"]
