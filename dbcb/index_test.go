package dbcb_test

import (
	"errors"
	"testing"

	"gitlab.com/phummarin.dam/utility/dbcb"
	"gitlab.com/phummarin.dam/utility/repositories"

	"github.com/couchbase/gocb"
)

func TestOpenBucketPool(t *testing.T) {
	type argsMock struct {
		bucketName, password string
		auth                 gocb.PasswordAuthenticator
		bucket               *gocb.Bucket
	}
	type args struct {
		userName, password, bucketName string
	}
	tests := []struct {
		name    string
		inParam *args
		inMock  *argsMock
		want    error
		wantErr bool
	}{
		// TODO: Add test cases.
		{
			name:    "case success",
			inParam: &args{"admin", "admin123", "Test"},
			inMock: &argsMock{"Test", "", gocb.PasswordAuthenticator{
				Username: "admin",
				Password: "admin123",
			}, &gocb.Bucket{}},
			want:    nil,
			wantErr: false,
		},
		{
			name:    "case fail",
			inParam: &args{"admin", "admin123", "Test"},
			inMock: &argsMock{"Test", "", gocb.PasswordAuthenticator{
				Username: "admin",
				Password: "admin123",
			}, nil},
			want:    errors.New("Error"),
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			ClusterInterfaceMock := new(repositories.ClusterInterfaceMock)
			ClusterInterfaceMock.On("OpenBucket", tt.inMock.bucketName, tt.inMock.password).Return(tt.inMock.bucket, tt.want)
			ClusterInterfaceMock.On("Authenticate", tt.inMock.auth).Return(tt.want)
			requestH := dbcb.RequestService{Cluster: ClusterInterfaceMock}
			err := requestH.OpenBucketPool(tt.inParam.userName, tt.inParam.password, tt.inParam.bucketName)
			if (err != nil) != tt.wantErr {
				t.Errorf("TestOpenBucketPool name = %v,Error %v, wantErr %v", tt.name, err, tt.wantErr)
				return
			}
		})
	}
}

func TestGetBucket(t *testing.T) {
	type argsMock struct {
		bucketName, password string
		bucket               *gocb.Bucket
	}
	type args struct {
		bucketName string
	}
	tests := []struct {
		name    string
		inParam *args
		inMock  *argsMock
		want    error
		wantErr bool
	}{
		// TODO: Add test cases.
		{
			name:    "case success",
			inParam: &args{"Test"},
			inMock:  &argsMock{"Test", "", &gocb.Bucket{}},
			want:    nil,
			wantErr: false,
		},
		{
			name:    "case fail",
			inParam: &args{"Test"},
			inMock:  &argsMock{"Test", "", nil},
			want:    errors.New("Error"),
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			ClusterInterfaceMock := new(repositories.ClusterInterfaceMock)
			ClusterInterfaceMock.On("OpenBucket", tt.inMock.bucketName, tt.inMock.password).Return(tt.inMock.bucket, tt.want)
			requestH := dbcb.RequestService{Cluster: ClusterInterfaceMock}
			_, err := requestH.GetBucket(tt.inParam.bucketName)
			if (err != nil) != tt.wantErr {
				t.Errorf("TestGetBucket name = %v,Error %v, wantErr %v", tt.name, err, tt.wantErr)
				return
			}
		})
	}
}

func TestTearDown(t *testing.T) {
	tests := []struct {
		name    string
		want    error
		wantErr bool
	}{
		// TODO: Add test cases.
		{
			name:    "case success",
			want:    nil,
			wantErr: false,
		},
		{
			name:    "case fail",
			want:    errors.New("Error"),
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			ClusterInterfaceMock := new(repositories.ClusterInterfaceMock)
			ClusterInterfaceMock.On("Close").Return(tt.want)
			requestH := dbcb.RequestService{Cluster: ClusterInterfaceMock}
			err := requestH.TearDown()
			if (err != nil) != tt.wantErr {
				t.Errorf("TestTearDown name = %v,Error %v, wantErr %v", tt.name, err, tt.wantErr)
				return
			}
		})
	}
}
