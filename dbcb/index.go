package dbcb

import (
	"gitlab.com/phummarin.dam/utility/model"
	"gitlab.com/phummarin.dam/utility/repositories"

	"github.com/couchbase/gocb"
)

var cluster *gocb.Cluster
var bucket *gocb.Bucket

type RequestService struct {
	Cluster repositories.ClusterInterface
}

func (c RequestService) Init(CbParam model.CouchBaseConnectParam) (*gocb.Cluster, error) {
	cluster, err := Connect(CbParam.Uri)
	if err != nil {
		return nil, err
	}
	c.Cluster = cluster
	c.OpenBucketPool(CbParam.User, CbParam.Password, CbParam.BucketName)
	return cluster, nil
}
func Connect(uri string) (*gocb.Cluster, error) {
	var err error
	cluster, err = gocb.Connect(uri)
	if err != nil {
		return nil, err
	}
	return cluster, nil
}
func (c RequestService) OpenBucketPool(user string, pass string, bucketName string) error {
	c.Cluster.Authenticate(gocb.PasswordAuthenticator{
		Username: user,
		Password: pass,
	})

	_, err := c.Cluster.OpenBucket(bucketName, "")
	if err != nil {
		return err
	}
	return nil
}
func (c RequestService) GetBucket(bucketName string) (*gocb.Bucket, error) {
	var err error
	bucket, err = c.Cluster.OpenBucket(bucketName, "")
	if err != nil {
		return nil, err
	}
	return bucket, nil
}
func (c RequestService) TearDown() error {
	if err := c.Cluster.Close(); err != nil {
		return err
	}
	return nil
}
