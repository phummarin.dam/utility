package model

type CouchBaseConnectParam struct {
	Uri        string
	User       string
	Password   string
	BucketName string
}
