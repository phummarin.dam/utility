package model

type HttpRequestParam struct {
	Request       interface{}
	Url           string
	ContentType   string
	Authorization string
	ClientCert    *string
	ClientKey     string
	ApiKey        string
	TimeoutSec    int
	Method        string
}
